import React, {Component, PropTypes} from 'react';
import {Link, IndexLink} from 'react-router';
import UserProfile from './UserProfile';
import './header.css';

export default class Header extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const {user} = this.props;
    const pathname = this.props.location.pathname;
    const isLoginPage = pathname.indexOf('login') > -1;
    const isConfirm = pathname.indexOf('confirm') > -1;
    const isRegister = pathname.indexOf('register') > -1;
    return (
      !isLoginPage && !isConfirm && !isRegister &&
      <div className="pos-f-t">
        <div className="collapse" id="navbar-header">
          <div className="container bg-inverse p-a-1"/>
        </div>
        <nav className="navbar navbar-light bg-faded navbar-fixed-top">
          <div className="container">
            <button type="button" className="navbar-toggle pull-xs-left hidden-sm-up" data-toggle="collapse"
                    data-target="#collapsingNavbar">&#9776;</button>
            <div id="collapsingNavbar" className="collapse navbar-toggleable-xs">
              <IndexLink to="/" className="navbar-brand">
                <div title="Home" className="brand"/>
                Home
              </IndexLink>
              <ul className="nav navbar-nav">
                <li title="Files" className='nav-item'><Link className="nav-link" to="/files">ListFiles</Link></li>
              </ul>
              <ul className="nav navbar-nav pull-xs-right">
                <UserProfile user={user} />
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

Header.propTypes = {
  user: PropTypes.string,
  location: React.PropTypes.object,
};
