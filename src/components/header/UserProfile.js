import React, {PropTypes} from 'react';
import {logoutUser, userPool} from '../../utils/awsUtils';

const UserProfile = () => {
  return (
    <li className="dropdown nav-item">
      <a href="#" className="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true"
         aria-expanded="false">
        <span className="fa fa-user"
              style={{marginRight: '0.5em'}}></span>{userPool && userPool.getCurrentUser() ? userPool.getCurrentUser().username : 'User'}<span
        className="caret"></span>
      </a>
      {userPool && userPool.getCurrentUser() &&
      <ul className="dropdown-menu" style={{right: 0, left: 'auto'}}>
        <a className="dropdown-item" href="#" onClick={() => {logoutUser()}}><i className="fa fa-sign-out"
                                                                      style={{marginRight: '0.5em'}}/>Log out</a>
      </ul>
      }
    </li>
  );
}

UserProfile.propTypes = {
  user: PropTypes.string
};

export default UserProfile;
