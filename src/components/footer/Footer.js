import React from 'react';

import './footer.css';

const Footer = () => (
  <footer className="footer">
    <div className="container-fluid">
      <p className="text-xs-center text-muted">
        Footer content will be added here!
      </p>
    </div>
  </footer>
);

export default Footer;
