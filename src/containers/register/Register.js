import React, { Component, PropTypes } from 'react';
import {registerUser} from '../../utils/awsUtils'

import './register.css';

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
  }

  handleRegister(event) {
    event.preventDefault();
    const userDetail= {
      username:this.refs.username.value,
      password: this.refs.password.value,
      name:this.refs.name.value,
      email:this.refs.email.value
    }
    registerUser(userDetail);
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4" style={{ float: 'none', margin: '0 auto' }}>
            <div className="card">
              <div className="card-header">Please Register</div>
              <form className="card-block">
                <div className="input-group">
                  <label>Name</label>
                  <input type="text" ref="name" className="form-control" placeholder="Name" required autoFocus/>
                </div>
                <div className="input-group">
                  <label>Email</label>
                  <input type="email" ref="email" className="form-control" placeholder="E-mail@domain.com" required/>
                </div>
                <div className="input-group">
                  <label>Username</label>
                  <input type="text" ref="username" className="form-control" placeholder="Username" required/>
                </div>
                <div className="input-group">
                  <label>Password</label>
                  <input type="password" ref="password" className="form-control" placeholder="Password needs to be minimum 8 char(one number + one special char)" required/>
                </div>
                <button className="btn btn-primary btn-block" onClick={this.handleRegister}>Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}