import React, {Component} from 'react';
import {confirmUser} from '../../utils/awsUtils';

export default class Confirm extends Component {
  constructor(props) {
    super(props);
    this.handleConfirm = this.handleConfirm.bind(this);
  }

  handleConfirm(event) {
    event.preventDefault();
    confirmUser(this.refs.username.value, this.refs.code.value);
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4" style={{float: 'none', margin: '0 auto'}}>

            <div className="card">
              <div className="card-header">Enter code form you email</div>
              <form className="card-block">
                <div className="input-group">

                  <input type="text" ref="username" className="form-control" placeholder="Username" required autoFocus/>
                </div>

                <div className="input-group">

                  <input type="code" ref="code" className="form-control" placeholder="Confirmation Code" required/>
                </div>
                <div className="col-md-6">
                  <button className="btn btn-primary btn-block" onClick={this.handleConfirm}>Submit</button>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    );
  }
}
