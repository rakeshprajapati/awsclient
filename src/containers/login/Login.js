import React, { Component, PropTypes } from 'react';
import {Link, hashHistory} from 'react-router';
import {authenticate} from '../../utils/awsUtils';

import './login.css';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin(event) {
    event.preventDefault();
    const username = this.refs.username;
    const password = this.refs.password;

    let user = {
     Username: username.value,
     Password: password.value
   }
    authenticate(user, (error, data) => {
      if(error) {
          return;
        }
      hashHistory.push('/');
    });
    username.value = '';
    password.value = '';
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4" style={{ float: 'none', margin: '0 auto' }}>
            <div className="card">
              <div className="card-header">Please Log in</div>
              <form className="card-block">
                <div className="input-group">
                  <span className="input-group-addon"><i className="fa fa-user"/></span>
                  <input type="text" ref="username" className="form-control" placeholder="Username (hint: admin)" required autoFocus/>
                </div>
                <div className="input-group">
                  <span className="input-group-addon"><i className="fa fa-lock"/></span>
                  <input type="password" ref="password" className="form-control" placeholder="Password (hint: password)" required/>
                </div>
                <div className="checkbox">
                  <label>
                    <input type="checkbox" value="remember-me"/> Remember me
                  </label>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <Link className="btn btn-primary btn-block" to="/register">Sign up</Link>
                  </div>
                  <div className="col-md-6">
                    <button className="btn btn-primary btn-block" onClick={this.handleLogin}><i className="fa fa-sign-in"/>{' '}Log in</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
