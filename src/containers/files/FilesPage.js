import React, {Component} from 'react';
import {Table, Column, Cell} from 'fixed-data-table';
import {listFiles} from '../../utils/awsUtils';

import './fixed-data-table-base.min.css';
import './fixed-data-table-style.min.css';
import './fixed-data-table.min.css';

const TextCell = ({rowIndex, data, col, ...props}) => (
  <Cell {...props}>
    {data[rowIndex][col]}
  </Cell>
);

export default class FilesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    listFiles((err, data) => {
      let strObject = JSON.stringify(data);
      if (err) {
        return;
      }
      this.setState({data: JSON.parse(strObject)});
    });
  }

  render() {
    let {data}  = this.state;
    return (
      <div className="container">
        {data && data.Contents && data.Contents.length &&
        <Table
          rowHeight={50}
          rowsCount={data.Contents.length}
          width={1100}
          height={500}
          headerHeight={50}>
          <Column
            header={<Cell>Name</Cell>}
            cell={<TextCell data={data.Contents} col="Key"/>}
            width={500}
            height={100}
          />
          <Column
            header={<Cell>Size</Cell>}
            cell={<TextCell data={data.Contents} col="Size"/>}
            width={100}
            height={100}
          />
          <Column
            header={<Cell>LastModified</Cell>}
            cell={<TextCell data={data.Contents} col="LastModified"/>}
            width={500}
            height={100}
          />
        </Table>
        }
        {!data && 'Loading data...'}
      </div>
    );
  }
}
