import React, {Component} from 'react';
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import {userPool} from '../../utils/awsUtils';
import {hashHistory} from 'react-router';

import './app.css';

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (!userPool || !userPool.getCurrentUser()) {
      hashHistory.push('/login');
    }
  }

  render() {
    const {user} = this.props;
    return (
      <div className="container">
        <Header location={this.props.location} user={user}/>
        <div className="container appContent">
          {this.props.children}
        </div>
        <Footer/>
      </div>
    );
  }
}
