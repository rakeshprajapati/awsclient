import React, {Component} from 'react';
import {upload} from '../../utils/awsUtils';

import './home.css';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload(event) {
    event.preventDefault();
    let file = this.refs.files.files[0];
    upload(file);
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-4" style={{float: 'none', margin: '0 auto'}}>
              <input type="file" ref="files" className="btn btn-primary btn-block"/>
              <button className="btn btn-primary btn-block" onClick={this.handleUpload}><i
                className="fa fa-upload"/>{' '}Upload
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
