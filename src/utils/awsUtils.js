import {
  CognitoUserPool,
  AuthenticationDetails,
  CognitoUser,
  CognitoUserAttribute
} from 'amazon-cognito-identity-js';
import AWS from 'aws-sdk';
import {hashHistory} from 'react-router';

const userPoolId = 'us-east-1_Nc9iV8Ghv';
const appClientId = '495pp82li0k3c9m0joc83221v7';
const identityPoolId = 'us-east-1:335e9380-3913-4bee-8088-cc411486c170';
const region = 'us-east-1';

export const session = {}

let poolData = {
  UserPoolId: userPoolId,
  ClientId: appClientId
}

AWS.config.region = region;

const loginId = `cognito-idp.${region}.amazonaws.com/${userPoolId}`;

export let userPool = new CognitoUserPool(poolData)

export function authenticate(user, callback) {
  let authDetails = new AuthenticationDetails(user);
  let userData = {
    Username: user.Username,
    Pool: userPool
  };
  let cognitoUser = new CognitoUser(userData);
  cognitoUser.authenticateUser(authDetails, {
    onSuccess: (result) => {
      setCredentials(result.getIdToken().getJwtToken());
      callback(null, result);
    },
    onFailure: (err) => {
      console.log(err);
    }
  });
}

export function updatePassword(user) {
  let userData = {
    Username: 'cnttest',
    Pool: userPool
  };
  let cognitoUser = new CognitoUser(userData);
  cognitoUser.changePassword('Cnt!1234', 'Cnt@1234', function (err, result) {
    if (err) {
      console.log(err);
      return;
    }
    console.log('call result: ' + result);
  });
}

export function registerUser(user) {
  let attributes = [];
  let emailData = {
    Name: 'email',
    Value: user.email
  };
  let nameData = {
    Name: 'name',
    Value: user.name
  };
  attributes.push(new CognitoUserAttribute(emailData));
  attributes.push(new CognitoUserAttribute(nameData));
  userPool.signUp(user.username, user.password, attributes, null, (err, result) => {
    if (err) {
      console.log(err);
      return;
    }
    hashHistory.push('/confirm');
    session.registered = true;
  });
}

export function confirmUser(username, code) {
  let userData = {
    Username: username,
    Pool: userPool
  };
  let cognitoUser = new CognitoUser(userData);
  cognitoUser.confirmRegistration(code, true, (err, result) => {
    if (err) {
      console.log(err);
      return;
    }
    hashHistory.push('/login');
  });
}

export function upload(file) {
  let params = {Key: file.name, ContentType: file.type, Body: file};
  let bucket = new AWS.S3({params: {Bucket: 'cntserverless-upload'}});
  bucket.upload(params, function (err, data) {
    if (err) {
      alert(err);
      return;
    }
    hashHistory.push('/files');
  });
}


export function listFiles(callback) {
  let bucket = new AWS.S3();
  bucket.listObjects({Bucket: 'cntserverless-upload'}, function (err, data) {
    if (err) {
      callback(err, null);
      return;
    }
    callback(null, data);
  });
}

function setCredentials(token) {
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: identityPoolId,
    Logins: {}
  });
  AWS.config.credentials.params.Logins[loginId] = token;
}

export function logoutUser() {
  let cognitoUser = userPool.getCurrentUser();
  if (cognitoUser != null) {
    cognitoUser.signOut();
    hashHistory.push('/login');
  }
}
